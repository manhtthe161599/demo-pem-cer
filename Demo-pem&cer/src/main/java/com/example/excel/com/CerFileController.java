package com.example.excel.com;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
public class CerFileController {

    @PostMapping("/uploadCerFile")
    public String uploadCerFile(@RequestParam("file") MultipartFile cerFile) {
        try {
            if (cerFile.isEmpty()) {
                return "file cer bị rỗng!!!";
            }
            byte[] cerData = cerFile.getBytes();
            String cerContent = new String(cerData, StandardCharsets.UTF_8);
            return "Dữ liệu của file cer:\n" + cerContent;
        } catch (IOException e) {
            return "Lỗi : " + e.getMessage();
        }
    }


}






