package com.example.excel.com;
import org.bouncycastle.util.io.pem.PemObject;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.bouncycastle.util.io.pem.PemReader;
import java.io.*;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/pem")
public class PemFileController {

    @PostMapping("/upload")
    public String uploadPemFile(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return "file pem bị rỗng!!!";
        }
        try {
            // Đọc dữ liệu từ file PEM
            Reader pemReader = new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8);
            PemReader pem = new PemReader(pemReader);
            StringBuilder pemContent = new StringBuilder();
            PemObject pemObject;
            while ((pemObject = pem.readPemObject()) != null) {
                pemContent.append(new String(pemObject.getContent(), StandardCharsets.UTF_8));
            }
            String pemData = pemContent.toString();
            return "Dữ liệu của file Pem:\n" + pemData;
        } catch (IOException e) {
            return "Lỗi: " + e.getMessage();
        }
    }
}


